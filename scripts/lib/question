#! /bin/bash

if [ "$ROOT" == "" ]; then
  echo "This is a dependency, it is not meant to be executed."
  exit 1
fi

if [ "$(color exists 2>/dev/null)" != "exists" ]; then
  source $ROOT/lib/colors
fi

# Shows a simple prompt and returns user answer
ask() {
  echo "" >&2
  printf " $(cG "?") $(cW --bold $1): " >&2
  read answer
  echo $answer
}

# Asks a yes/no question, defaults to yes
askYesNo() {
  local answer="$(ask "$1 $(cW --faint "[Y/n]")" | tr '[:upper:]' '[:lower:]')"
  if [ "$answer" == "" ] || [ "$answer" == "y" ] || [ "$answer" == "yes" ]; then
    echo "y"
  else
    echo "n"
  fi
}

# Asks a yes/no question, defaults to no
askNoYes() {
  local answer="$(ask "$1 $(cW --faint "[y/N]")" | tr '[:upper:]' '[:lower:]')"
  if [ "$answer" == "y" ] || [ "$answer" == "yes" ]; then
    echo "y"
  else
    echo "n"
  fi
}

# Shows a list of choices and return selected option
choice() {
  local prompt="$(cG "!") $(color --bold $1)"
  echo "" >&2
  printf " $prompt\n" >&2
  shift
  
  local num=0
  for choice in "$@"; do
    num=$((num + 1))
    printf "   $(cC "$num.") $choice\n" >&2
  done

  local answer="$(ask "Enter a value between $(cC 1) and $(cC $num)")"
  
  re='^[0-9]+$'
  if ! [[ $answer =~ $re ]]; then
    exit 1
  fi

  if [[ $answer -lt 1 ]] || [[ $answer -gt $num ]]; then
    exit 1
  fi

  remove_lines $((num + 3)) >&2
  printf " $prompt $(cM ${!answer})\n" >&2
  echo $answer
}

remove_lines() {
  $ROOT/lib/ansi --up=$1 --no-newline
  $ROOT/lib/ansi --delete-lines=$1 --no-newline
}
