#! /bin/bash

ROOT="$(cd -- "$(dirname -- "${BASH_SOURCE[0]:-$0}")" &>/dev/null && pwd 2>/dev/null)"

source $ROOT/lib/banner
source $ROOT/lib/question
source $ROOT/lib/logger

show_banner_version "CedSharp's DotFiles Installer" "v0.1"

echo ""
log "This installer will guide you through installing the different"
log "features available in CedSharp's DotFiles."

quit=0
while [[ $quit -eq 0 ]]; do
  case "$(choice "What would you like to do?" \
    "Configure terminal"\
    "Configure development tools" \
    "I'm done, Quit"\
  )" in
    1) source $ROOT/actions/setup_terminal;;
    2) source $ROOT/actions/install_devtools;;
    *) quit=1; echo -e "   Have a nice day!\n";;
  esac

  if [[ $quit -eq 0 ]]; then
    show_banner "CedSharp's DotFiles Installer"
  fi
done

