# CedSharp's DotFiles - v2

My attempt at making a better dotfile installation experience.
I also wanted to learn more about bash.

## Installing JetBrainsMono from Nerd Fonts

![Installing Jetbrains](screenshots/install_jetbrains.png)

This command simply downloads a zip file from the nerd fonts
repository, extracts the fonts, moves them to `$HOME/.local/share/fonts`
and updates the font cache.

This way you don't have to do it yourself!
