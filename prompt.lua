-- require 'ansicolor'
local lfs = require 'lfs'

IFOLDER = ''
INODE = ''
INPM = ''
IPYTHON = ''
ISPREAD = ''
IBRANCH = ''
IARROW = ''

-- Helper: Retrieve R, G, B from '#hexnum'
local function hexToRgb(hex)
	hex = hex:gsub("#", "")
	return tonumber('0x'..hex:sub(1,2)), tonumber('0x'..hex:sub(3,4)), tonumber('0x'..hex:sub(5,6))
end

local function escape()
	return string.char(27)
end

local function cr()
	return '\001' .. escape() .. '[0m' .. '\002'
end

local function cfg(hex)
	local r, g, b = hexToRgb(hex)
	return '\001' .. escape() .. '[38;2;' .. r .. ';' .. g .. ';' .. b .. 'm' .. '\002'
end

local function cbg(hex)
	local r, g, b = hexToRgb(hex)
	return '\001' .. escape() .. '[48;2;' .. r .. ';' .. g .. ';' .. b .. 'm' .. '\002'
end

-- Helper: Split string by delimiter
local function split(str, delimiter)
	delimiter = delimiter or ','
	local t = {}
	if str ~= nil and str ~= '' then
		for match in string.gmatch(str, '([^'..delimiter..']+)') do
			table.insert(t, match)
		end
	end
	return t
end

-- Helper: Exec a shell command, capture output and close handle
local function exec(cmd)
	local handle = assert(io.popen(cmd, 'r'))
	local result = assert(handle:read('*a'))
	handle:close()
	return result:gsub('^%s+', ''):gsub('%s+$', ''):gsub('[\n\r]+', ' ')
end

-- Structure for managing the segments of the prompt
P = { segments = {}, }
P.add = function(text, fg, bg)
	P.segments[#P.segments+1] = { text=text, fg=fg, bg=bg }
end

P.print = function()
	for i, s in ipairs(P.segments) do
		if i > 1 then
			local before = cfg(P.segments[i-1].bg)
			local after = cbg(P.segments[i].bg)
			io.write(cr() .. before .. after .. IARROW)
		end

		local bg = cbg(P.segments[i].bg)
		local fg = cfg(P.segments[i].fg)
		local text = ' ' .. P.segments[i].text .. ' '
		io.write(cr() .. bg .. fg .. text)
	end

	-- Add trailing arrow
	local before = cfg(P.segments[#P.segments].bg)
	io.write(cr() .. before .. IARROW .. cr())

	-- Add newline where cursor will be
	io.write('\n ' .. '\u{f554}' .. ' ')
end

local function get_git_info()
	local branch = exec([[git branch 2>/dev/null | grep '*' | sed 's/* //']])
	if branch == '' then return nil end

	local root = exec([[git rev-parse --show-toplevel 2>/dev/null]])
	if root == '' then return nil end

	return string.gsub(root, '%s+', ''), string.gsub(branch, '%s+', '')
end

local function get_npm_info(root)
	if root == '' or root == nil then return nil end

	local package = exec('cat ' .. root .. '/package.json 2>/dev/null')
	if package == nil then return nil end

	local name = select(3, package:find([[%s*"name":%s*"(..-)",?%s*]]))
	if name == nil then return nil end

	local version = exec('node --version')

	return name, version
end

local function get_python_info(root)
	local venv=''
	local venv_path = os.getenv('VIRTUAL_ENV')
	local venv_parts = split(venv_path, '/')
	local venv_name = venv_parts[#venv_parts] or ''
	venv = split(venv_name, '-')[1] or ''

	local get_py = false
	if venv ~= '' then get_py = true end

	if get_py == false and root ~= nil and root ~= '' then
		if exec('cat ' .. root .. '/requirements.txt 2>/dev/null') ~= '' then
			get_py = true
		elseif exec('cat ' .. root .. '/PipFile 2>/dev/null') ~= '' then
			get_py = true
		end
	end

	local py_version = ''
	if get_py == true then
		py_version = exec('python --version 2>/dev/null')
		if py_version == '' then py_version = exec('python3 --version 2>/dev/null') end
		if py_version == '' then py_version = exec('python2 --version 2>/dev/null') end
	end

	return py_version:gsub('Python ', ''), venv
end

local function get_cwd_info(root)
	local cwd = lfs.currentdir()

	if root ~= nil then
		if root:sub(-1) == '/' then root = cwd.sub(string.len(cwd) -1) end
		cwd = cwd:gsub(root:match('.*/'), '')
	else
		cwd = cwd:gsub(os.getenv('HOME'), '~')
	end

	local parts = split(cwd, '/')
	if #parts > 3 then
		cwd = ISPREAD .. '/' .. parts[#parts+1 - 3] .. '/' .. parts[#parts+1 - 2] .. '/' .. parts[#parts+1 - 1]
	end

	return cwd
end

local root, branch = get_git_info()
local npm_name, node_version = get_npm_info(root)
local py_version, venv = get_python_info(root)
local cwd_path = get_cwd_info(root)

local cwd = { fg='#d4d4d4', bg='#484848' }
local npm = { fg='#d4d4d4', bg='#3030ee' }
local py = { fg='#e4e4e4', bg='#40a044' }
local git = { fg='#141414', bg='#ffff00' }

P.add(IFOLDER .. ' ' .. cwd_path, cwd.fg, cwd.bg)
if npm_name ~= nil then
	P.add(INPM .. ' ' .. npm_name .. ' ' .. INODE .. ' ' .. node_version, npm.fg, npm.bg)
end
if py_version ~= '' and py_version ~= nil then
	local py_text = IPYTHON .. ' ' .. py_version
	if venv ~= nil and venv ~= '' then py_text = '(' .. venv .. ') ' .. py_text end
	P.add(py_text, py.fg, py.bg)
end
if branch ~= nil then
	P.add(IBRANCH .. ' ' .. branch, git.fg, git.bg)
end

P.print()

